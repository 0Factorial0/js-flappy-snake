# flappy-snake

## Draft
<div width="99%">
    <img width="99%" src="https://gitlab.com/0Factorial0/js-flappy-snake/-/raw/master/content/draft.png">
</div>

## Demo
<div width="99%">
    <img width="99%" src="https://gitlab.com/0Factorial0/js-flappy-snake/-/raw/master/content/demo.png">
</div>

## All The Demo Videos So Far

1. https://www.youtube.com/watch?v=PH3AWCqpcg8

2. https://www.youtube.com/watch?v=5dGiFsOjNOE

3. https://www.youtube.com/watch?v=T2IaBYk_CMA

4. https://www.youtube.com/watch?v=dWG8qnB-thw

5. https://www.youtube.com/watch?v=MEVeNA_NbPw

6. https://www.youtube.com/watch?v=8P43bg3m2vo

7. https://www.youtube.com/watch?v=prHtWHNjEnY

8. https://www.youtube.com/watch?v=9IUd9m2_pqg

9. https://www.youtube.com/watch?v=ES5H7S2RmbQ

10. https://www.youtube.com/watch?v=RiibuIFhWmE

#0x0 #mohurine